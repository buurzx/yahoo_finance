Rails.application.routes.draw do
  root to: 'visitors#index'
  devise_for :users
  resources :users, only: [:show, :index] do
    put 'add_stocks' => "users#add_stocks"
    get 'delete_stock' => "users#delete_stock"
  end
  resources :stocks, only: [:new, :create]
end
