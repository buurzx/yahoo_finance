class CreateStocks < ActiveRecord::Migration
  def change
    create_table :stocks do |t|
      t.string :title
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
