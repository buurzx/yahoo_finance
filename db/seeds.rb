# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# user = CreateAdminService.new.call
# puts 'CREATED ADMIN USER: ' << user.email
stocks = Stock.create([
  { title: "AAPL" },
  { title: "QQQ" },
  { title: "DOW" },
  { title: "BAC" },
  { title: "MU" },
  { title: "SUNE" },
  { title: "VXX" },
  { title: "PBR" },
  { title: "GE" },
  { title: "TWTR" },
  { title: "VALE" },
  { title: "MSFT" },
  { title: "CSCO" },
  { title: "SIRI" },
  { title: "FCX" },
  { title: "PFE" }
  ])
