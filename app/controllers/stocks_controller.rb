class StocksController < ApplicationController
  before_action :authenticate_user!

  def create
    @stock = Stock.create(stock_params)
    redirect_to :back, success: "Акция добавлена"
  end

  def new
    @stock = Stock.new
  end

  private

  def stock_params
    params.require(:stock).permit(:title)
  end

end
