class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    @users = User.all
  end

  def show
    @data = yahoo_stock_data
    @user = User.find(params[:id])

    unless @user == current_user
      redirect_to :back, :alert => "Access denied."
    end
  end

  def add_stocks
    @stoks = Stock.where(id: params[:stock_ids])
    current_user.stocks << @stoks

    if current_user.save
      redirect_to :back, notice: "Портфель обновлен"
    else
      redirect_to :back, alert: "Произошла ошибка"
    end
  end

  def delete_stock
    @stock = Stock.find params[:stock_id]

    if current_user.stocks.delete(@stock)
      redirect_to :back, notice: "Портфель обновлен"
    else
      redirect_to :back, alert: "Произошла ошибка"
    end
  end

end
