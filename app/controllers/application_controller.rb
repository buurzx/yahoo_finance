class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def yahoo_stock_data
    yahoo_client = YahooFinance::Client.new
    yahoo_client.quotes(current_user.stocks.map { |stock| stock.title } , [:bid, :change, :change_in_percent])
  end

  protected

  def after_sign_up_path_for(resource)
    user_path(current_user)
  end

  def after_sign_in_path_for(resource)
    user_path(current_user)
  end
end
