class VisitorsController < ApplicationController
  def index
    if current_user
      redirect_to current_user, notice: "Вы уже в системе."
    end
  end
end
