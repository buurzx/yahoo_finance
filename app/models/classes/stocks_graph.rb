class StocksGraph

  def initialize(stocks_name)
    @stocks_name = stocks_name
  end

  def two_year_graphic
    hash = {}

    # fill hash with stock data
    stocks_data.each do |a|
      a.each do |stock|
        #  2 year history stock data
        if Date.parse("#{ stock.trade_date }") > Date.new(Date.today.year - 2, Date.today.mon, Date.today.mday)
          hash["#{stock.trade_date}"] =+ stock.close.to_f
        end
      end
    end

    hash
  end

  def stocks_data
    array = []

    # create yahoo instance
    yahoo_client = YahooFinance::Client.new

    # get stock data by given symbols and fill array with it
    @stocks_name.each do |n|
      array << yahoo_client.historical_quotes(n, { raw: false, period: :monthly } )
    end
    array
  end

end
