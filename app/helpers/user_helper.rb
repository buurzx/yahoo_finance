module UserHelper
  # get array of stock's names
  def user_stocks_title
    current_user.stocks.map { |stock| stock.title }
  end
end
