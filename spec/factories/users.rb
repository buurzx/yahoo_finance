FactoryGirl.define do
  factory :user do
    password "password"
    sequence(:email) { |n| "johndoe#{n}@example.com"}

    factory :user_with_stocks do
      ignore do
        stocks_count 5
      end

      after(:create) do |user, evaluator|
        create_list(:stock, evaluator.stocks_count, user: user)
      end
   end
  end
end
