require 'rails_helper'

describe StocksController do
  context "authentication" do
    it "blocks unauthenticated access" do
      sign_in nil
      get :new
      expect(response).to redirect_to(new_user_session_path)
    end

    it "allows authenticated access" do
      sign_in
      get :new
      expect(response).to be_success
    end
  end

end
