require 'rails_helper'

describe UsersController do

  context "authentication" do
    it "blocks unauthenticated access" do
      sign_in nil
      get :index
      expect(response).to redirect_to(new_user_session_path)
    end

    it "allows authenticated access" do
      sign_in
      get :index
      expect(response).to be_success
    end
  end

  describe "PUT #add_stocks" do
    before :each do
      @user = FactoryGirl.create(:user_with_stocks)
      sign_in @user
      @stock = FactoryGirl.create(:stock, user: @user)
      @request.env['HTTP_REFERER'] = user_path(@user)
      Rails.application.routes.draw do
        get '/users/:id', controller: 'users', action: :show, as: :user
        put 'add_stocks', controller: 'users', action: :add_stocks
      end
    end

    after do
      Rails.application.reload_routes!
    end

    it "must increase size of stocks" do
      expect{ put :add_stocks, stock_ids: @stock.id, id: @user.id }.to change{ @user.stocks.length }.by(1)
    end

    it "redirects to the user" do
      put :add_stocks, stock_ids: @stock.id, id: @user.id
      expect(response).to redirect_to(user_path(@user))
    end
  end

  describe "GET #delete_stock" do
    before :each do
      @user = FactoryGirl.create(:user_with_stocks)
      sign_in @user
      @stock = FactoryGirl.create(:stock, user: @user)
      @request.env['HTTP_REFERER'] = user_path(@user)
      Rails.application.routes.draw do
        get '/users/:id', controller: 'users', action: :show, as: :user
        get 'delete_stock', controller: 'users', action: :delete_stock
      end
    end

    after do
      Rails.application.reload_routes!
    end

    it "must decrease size of stocks" do
      expect{ get :delete_stock, stock_id: @stock.id }.to change{ @user.stocks.length }.by(-1)
    end

    it "redirects to the user" do
      get :delete_stock, stock_id: @stock.id
      expect(response).to redirect_to(user_path(@user))
    end
  end


end
