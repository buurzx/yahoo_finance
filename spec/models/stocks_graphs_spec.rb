require 'rails_helper'

describe StocksGraph do

  describe "stocks_data" do
    subject { StocksGraph.new(["TST", "XMPL"]).stocks_data }
    let(:uri) { 'http://ichart.finance.yahoo.com/table.csv?g=m&ignore=.csv&s=TST' }
    before    { stub_request(:get, uri).to_return(:status => 200, :body => "stock_data.json") }
    before    { stub_request(:get, "http://ichart.finance.yahoo.com/table.csv?g=m&ignore=.csv&s=XMPL").
         with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => "stock_data.json", :headers => {}) }
    context "must be a Array" do
      it { should be_a(Array) }
    end
  end

  describe "two_year_graphic" do
    subject { StocksGraph.new(["TST", "XMPL"]).two_year_graphic }
    let(:uri) { 'http://ichart.finance.yahoo.com/table.csv?g=m&ignore=.csv&s=TST' }
    before    { stub_request(:get, uri).to_return(:status => 200, :body => "stock_data.json") }
    before    { stub_request(:get, "http://ichart.finance.yahoo.com/table.csv?g=m&ignore=.csv&s=XMPL").
         with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => "stock_data.json", :headers => {}) }
    context "must be a Hash" do
      it { should be_a(Hash) }
    end
  end

end
